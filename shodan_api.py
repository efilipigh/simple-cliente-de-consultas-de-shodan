import requests

# import json

tu_api_de_shodan = "aca poner la api que provee shodan"

url_base = "https://api.shodan.io"
print("Que deseas buscar?\n")
print("1-IP")
print("2-Contar resultados basados en un Dork")
print("3-Objetivos basados en un Dork")
print("4-Mostrar filtros posibles para aplicar a busqueda")
print("5-Mostrar puertos los cuales estan disponibles")
opcion = int(input())

if opcion == 1:
    ip_obj = input("Dame la ip a investigar\n")
    comp_ip = "/shodan/host/" + ip_obj + "?key=" + str(tu_api_de_shodan)
    url = url_base + comp_ip
    print(url)
    respuesta = requests.get(url, stream=True).json()
    # filtrado_json = json.loads(respuesta.text)
    # for x in filtrado_json:
    for x in respuesta:
        # print("%s: %s" % (x, filtrado_json[x]))
        print("%s: %s" % (x, respuesta[x]))
    # print(respuesta)
elif opcion == 2:
    opt_obj = input("Dame el dork a usar\n")
    filt_obj = input("Dame el filtro a usar\n")
    # /shodan/host/count?key={YOUR_API_KEY}&query={query}&facets={facets}
    comp_ip = "/shodan/host/count?key=" + tu_api_de_shodan + "&query=" + opt_obj + "&facets=" + filt_obj
    url = url_base + comp_ip + opt_obj + filt_obj
    print(url)
    respuesta = requests.get(url, stream=True).json()
    # filtrado_json = json.loads(respuesta.text)
    # for x in filtrado_json:
    for x in respuesta:
        # print("%s: %s" % (x, filtrado_json[x]))
        print("%s: %s" % (x, respuesta[x]))
    # print(respuesta)
elif opcion == 3:
    # https://api.shodan.io/shodan/host/search?key={YOUR_API_KEY}&query={query}&facets={facets}
    opt_obj = input("Dame el dork a usar\n")
    filt_obj = input("Dame el filtro a usar\n")
    # /shodan/host/search?key={YOUR_API_KEY}&query={query}&facets={facets}
    comp_url = "/shodan/host/count?key=" + tu_api_de_shodan + "&query=" + opt_obj + "&facets=" + filt_obj
    url = url_base + comp_url + opt_obj + filt_obj
    print(url)
    respuesta = requests.get(url, stream=True).json()
    # filtrado_json = json.loads(respuesta.text)
    # for x in filtrado_json:
    for x in respuesta:
        # print("%s: %s" % (x, filtrado_json[x]))
        print("%s: %s" % (x, respuesta[x]))
    # print(respuesta)
elif opcion == 4:
    # /shodan/host/search?key={YOUR_API_KEY}&query={query}&facets={facets}
    comp_filtro = "/shodan/host/search/facets?key=" + tu_api_de_shodan
    url = url_base + comp_filtro
    print(url)
    respuesta = requests.get(url, stream=True).json()
    # filtrado_json = json.loads(respuesta.text)
    # for x in filtrado_json:
    # for x in respuesta:
    # print("%s: %s" % (x, filtrado_json[x]))
    # print('%d: %d' % (x, respuesta[x]))
    # print(x, respuesta[x])
    for x in range(len(respuesta)):
        print(respuesta[x]),
    #print(respuesta)
elif opcion == 5:
    # https://api.shodan.io/shodan/ports?key={YOUR_API_KEY}
    comp_filtro = "/shodan/ports?key=" + tu_api_de_shodan
    url = url_base + comp_filtro
    print(url)
    respuesta = requests.get(url, stream=True).json()
    # filtrado_json = json.loads(respuesta.text)
    # for x in filtrado_json:
    for x in respuesta:
        # print("%s: %s" % (x, filtrado_json[x]))
        print("%s: %s" % (x, respuesta[x]))
    # print(respuesta)
